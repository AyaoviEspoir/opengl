# This Makefile requires GNU make, which is called gmake on Solaris systems
#
# 'make'        : clean, build and run practical2
# 'make clean'  : remove build products
# 'make run'    : run practical2

PROG = bin/practical2

OBJS = obj/*

ARCH=$(shell uname -m)
OS=$(shell uname -s)

CC = gcc

LDLIBS = -L/usr/lib -lm -ljpeg -lX11 -lGL

INCLUDE = -ITools -ILibraries -ILibraries/GL

DEFINE = -DGLEW_NO_GLU

.PHONY: all default clean run

all: clean practical2 run

practical2: 
	$(CC) $(DEFINE) $(INCLUDE) -c main.c -o obj/main.o
	$(CC) $(DEFINE) $(INCLUDE) -c Libraries/glew.c -o obj/glew.o
	$(CC) $(DEFINE) $(INCLUDE) -c Tools/Global.c -o obj/Global.o
	$(CC) $(DEFINE) $(INCLUDE) -c Tools/OpenGL_Wrapper.c -o obj/OpenGL_Wrapper.o
	$(CC) $(DEFINE) $(INCLUDE) -c Tools/JPEG_Wrapper.c -o obj/JPEG_Wrapper.o
	$(CC) -o bin/practical2 obj/main.o obj/glew.o obj/Global.o obj/OpenGL_Wrapper.o obj/JPEG_Wrapper.o $(LDLIBS)
 
clean:
	rm -f -R bin/*
	rm -f -R obj/*

run:
	bin/practical2
