#ifndef Global_h
#define Global_h
//------------------------------------------------------------------------------

#ifndef bool
 #define bool  char
 #define true  1
 #define false 0
#endif
//------------------------------------------------------------------------------

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
//------------------------------------------------------------------------------

#include "glew.h"
//------------------------------------------------------------------------------

#ifdef __WIN32__
#include "wglew.h"
#include <windows.h>

HWND hwnd;
HDC  hDC;

#define Key_Escape VK_ESCAPE
#define Key_J 'J'
#define Key_K 'K'
#define Key_I 'I'
#define Key_O 'O'
#endif // __WIN32__
//------------------------------------------------------------------------------

#ifdef __unix__
#include <sys/time.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/glx.h>

#define Key_Escape 9
#define Key_J 44
#define Key_K 45
#define Key_I 31
#define Key_O 32
#define Key_E 26
#endif // __unix__
//------------------------------------------------------------------------------

void Message(const char* s);
double GetClock(); // Returns the clock ticks in seconds
//------------------------------------------------------------------------------

#endif
//------------------------------------------------------------------------------
