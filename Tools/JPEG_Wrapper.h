#ifndef JPEG_Wrapper_h
#define JPEG_Wrapper_h
//------------------------------------------------------------------------------

#include "Global.h"
#include "jpeglib.h"
//------------------------------------------------------------------------------

typedef struct JPEG_TAG{
 unsigned Width;
 unsigned Height;

 unsigned char* Image;
} JPEG;
//------------------------------------------------------------------------------

JPEG* JPEG_New   (unsigned Width, unsigned Height);
void  JPEG_Delete(JPEG* Image);

JPEG* JPEG_Read (const char* Filename);
bool  JPEG_Write(const char* Filename, JPEG* Image);
//------------------------------------------------------------------------------

#endif
//------------------------------------------------------------------------------
