#include "Global.h"
//------------------------------------------------------------------------------

void Message(const char* s){
 printf("%s\n", s);
}
//------------------------------------------------------------------------------

double GetClock(){
 #ifdef __WIN32__
  return GetTickCount() / 1000.0;
 #endif
 #ifdef __unix__
  struct timeval Time;
  struct timezone Zone;
  gettimeofday(&Time, &Zone);
  return (double)Time.tv_sec + (double)Time.tv_usec/1e6;
 #endif
}
//------------------------------------------------------------------------------

