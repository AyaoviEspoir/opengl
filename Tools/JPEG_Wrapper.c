#include "JPEG_Wrapper.h"
//------------------------------------------------------------------------------

JPEG* JPEG_New(unsigned Width, unsigned Height){
 JPEG* Image = (JPEG*)malloc(sizeof(JPEG));

 Image->Width  = Width;
 Image->Height = Height;

 Image->Image = (unsigned char*)malloc(
  Width*Height*3*sizeof(unsigned char)
 );
 if(!Image->Image){
  printf("Out of memory.\n");
  free(Image);
  return 0;
 }
 return Image;
}
//------------------------------------------------------------------------------

void JPEG_Delete(JPEG* Image){
 if(Image){
  free(Image->Image);
  free(Image);
 }
}
//------------------------------------------------------------------------------

JPEG* JPEG_Read(const char* Filename){
 JPEG* Image;
 FILE* File;

 struct jpeg_error_mgr         jerr;
 struct jpeg_decompress_struct cinfo;

 if((File = fopen(Filename, "rb")) == 0){
  fprintf(stderr, "cannot open %s\n", Filename);
  return 0;
 }

 cinfo.err = jpeg_std_error(&jerr);

 jpeg_create_decompress(&cinfo);
 jpeg_stdio_src        (&cinfo, File);
 jpeg_read_header      (&cinfo, true);
 jpeg_start_decompress (&cinfo);

 Image = JPEG_New(cinfo.output_width, cinfo.output_height);

 JSAMPROW Scanline;
 while(cinfo.output_scanline < Image->Height){
  Scanline = Image->Image + cinfo.output_scanline*Image->Width*3;
  jpeg_read_scanlines(&cinfo, &Scanline, 1);
 }

 jpeg_finish_decompress (&cinfo);
 jpeg_destroy_decompress(&cinfo);

 fclose(File);

 return Image;
}
//------------------------------------------------------------------------------

bool JPEG_Write(const char* Filename, JPEG* Image){
 FILE* File;

 int quality;

 struct jpeg_error_mgr       jerr;
 struct jpeg_compress_struct cinfo;

 if((File = fopen(Filename, "wb")) == NULL){
  fprintf(stderr, "cannot open %s\n", Filename);
  return false;
 }

 cinfo.err = jpeg_std_error(&jerr);

 jpeg_create_compress(&cinfo);
 jpeg_stdio_dest     (&cinfo, File);

 cinfo.image_width      = Image->Width;
 cinfo.image_height     = Image->Height;
 cinfo.input_components = 3;
 cinfo.in_color_space   = JCS_RGB;

 jpeg_set_defaults(&cinfo);

 quality = 70;
 jpeg_set_quality   (&cinfo, quality, TRUE);
 jpeg_start_compress(&cinfo, TRUE);

 JSAMPROW Scanline;
 while(cinfo.next_scanline < cinfo.image_height){
  Scanline = Image->Image + cinfo.next_scanline*Image->Width*3;
  jpeg_write_scanlines(&cinfo, &Scanline, 1);
 }

 jpeg_finish_compress (&cinfo);
 fclose(File);
 jpeg_destroy_compress(&cinfo);

 return true;
}
//------------------------------------------------------------------------------
