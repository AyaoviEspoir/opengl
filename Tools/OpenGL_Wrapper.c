#include "OpenGL_Wrapper.h"
//------------------------------------------------------------------------------

unsigned GL_Version;
unsigned GLSL_Version;

unsigned ImageWidth;
unsigned ImageHeight;

int Toggle 	   = 0;
float Zoom     = 1.0;
int   Variable = 21;
//------------------------------------------------------------------------------

static GLuint hProgram;
static GLint  aVertex;
static GLint  aTexture;

static GLint  uWidth;
static GLint  uHeight;
       GLint  uZoom;
       GLint  uToggle;
       GLint  uTime;
       GLint  uVariable;
static GLint  uImage;

static GLuint VertexBuffer;
//------------------------------------------------------------------------------

bool LoadShader(const char* VertexFile, const char* FragmentFile){
 // Read the vertex program file
 FILE* File = fopen(VertexFile, "rb");
 if(!File){
  Message("Cannot open vertex program");
  return false;
 }

 // Get file size
 int FileSize;
 fseek(File, 0, SEEK_END);
 FileSize = ftell(File);
 fseek(File, 0, SEEK_SET);

 // Allocate source buffer
 char* Source = (char*)malloc((FileSize+1)*sizeof(char));
 if(!Source){
  printf("Error: Cannot allocate memory for source buffer.\n");
  fclose(File);
  return false;
 }

 // Read the file
 int j = 0;
 int c = fgetc(File);
 while(c != EOF){
  Source[j++] = (char)c;
  c = fgetc(File);
 }
 fclose(File);
 Source[j] = 0;

 // Create shader handle and load the source into the handle
 GLuint hVertexShader   = glCreateShader(GL_VERTEX_SHADER  );
 glShaderSource(hVertexShader, 1, (const GLchar**)&Source, &FileSize);
 free(Source);

 // Read the fragment program file
 File = fopen(FragmentFile, "rb");
 if(!File){
  Message("Cannot open fragment program");
  return false;
 }

 // Get file size
 fseek(File, 0, SEEK_END);
 FileSize = ftell(File);
 fseek(File, 0, SEEK_SET);

 // Allocate source buffer
 Source = (char*)malloc((FileSize+1)*sizeof(char));
 if(!Source){
  printf("Error: Cannot allocate memory for source buffer.\n");
  fclose(File);
  return false;
 }

 // Read the file
 j = 0;
 c = fgetc(File);
 while(c != EOF){
  Source[j++] = (char)c;
  c = fgetc(File);
 }
 fclose(File);
 Source[j] = 0;

 // Create shader handle and load the source into the handle
 GLuint hFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
 glShaderSource(hFragmentShader, 1, (const GLchar**)&Source, &FileSize);
 free(Source);

 // Compile and check for errors
 glCompileShader(hVertexShader);

 GLint Result;
 glGetShaderiv(hVertexShader, GL_COMPILE_STATUS, &Result);
 if(!Result){
  char s[0x400];
  glGetShaderInfoLog(hVertexShader, 0x400, 0, s);
  Message(s);
  glDeleteShader(hVertexShader  );
  glDeleteShader(hFragmentShader);
  return false;
 }

 glCompileShader(hFragmentShader);

 glGetShaderiv(hFragmentShader, GL_COMPILE_STATUS, &Result);
 if(!Result){
  char s[0x400];
  glGetShaderInfoLog(hFragmentShader, 0x400, 0, s);
  Message(s);
  glDeleteShader(hVertexShader  );
  glDeleteShader(hFragmentShader);
  return false;
 }

 // Link and check for errors
 hProgram = glCreateProgram();
 glAttachShader(hProgram, hVertexShader);
 glAttachShader(hProgram, hFragmentShader);

 glLinkProgram(hProgram);

 glDeleteShader(hVertexShader);
 glDeleteShader(hFragmentShader);

 glGetProgramiv(hProgram, GL_LINK_STATUS, &Result);
 if(!Result){
  char s[0x400];
  glGetProgramInfoLog(hProgram, 0x400, 0, s);
  Message(s);
  glDeleteProgram(hProgram);
  return false;
 }

 glEnableVertexAttribArray(0);
 glEnableVertexAttribArray(1);

 aVertex  = glGetAttribLocation(hProgram, "Vertex" );
 aTexture = glGetAttribLocation(hProgram, "Texture");

 uWidth    = glGetUniformLocation(hProgram, "Width" );
 uHeight   = glGetUniformLocation(hProgram, "Height");
 uToggle   = glGetUniformLocation(hProgram, "Toggle");
 uZoom     = glGetUniformLocation(hProgram, "Zoom");
 uTime     = glGetUniformLocation(hProgram, "Time");
 uVariable = glGetUniformLocation(hProgram, "Variable");
 uImage    = glGetUniformLocation(hProgram, "Image" );

 glUseProgram(hProgram);

 glGenBuffers(1, &VertexBuffer);
 glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);

 glVertexAttribPointer(
  aVertex, 2, GL_FLOAT, GL_FALSE,
  4*sizeof(GLfloat),
  0
 );

 glVertexAttribPointer(
  aTexture, 2, GL_FLOAT, GL_FALSE,
  4*sizeof(GLfloat),
  (const GLvoid*)(2*sizeof(GLfloat))
 );

 return true;
}
//------------------------------------------------------------------------------

unsigned Interpolate(JPEG* Image, double x, double y){
 unsigned Output = 0xFF;

 x =      x *(Image->Width -1);
 y = (1.0-y)*(Image->Height-1);

 int c;

 int fx = floor(x); int cx = ceil(x);
 int fy = floor(y); int cy = ceil(y);

 if(fx < 0) fx = 0;
 if(cx < 0) cx = 0;
 if(fy < 0) fy = 0;
 if(cy < 0) cy = 0;

 if(fx >= Image->Width ) fx = Image->Width -1;
 if(cx >= Image->Width ) cx = Image->Width -1;
 if(fy >= Image->Height) fy = Image->Height-1;
 if(cy >= Image->Height) cy = Image->Height-1;

 if(fx == cx){
  if(fx == 0) cx = fx+1;
  else        fx = cx-1;
 }
 if(fy == cy){
  if(fy == 0) cy = fy+1;
  else        fy = cy-1;
 }

 for(c = 2; c >= 0; c--){
  Output <<= 8;
  Output |= 0xFF & (unsigned)round((
   (1.0 - fabs(fx - x))*(1.0 - fabs(fy - y))*
          Image->Image[(fy*Image->Width + fx)*3 + c] +
   (1.0 - fabs(cx - x))*(1.0 - fabs(fy - y))*
          Image->Image[(fy*Image->Width + cx)*3 + c] +
   (1.0 - fabs(fx - x))*(1.0 - fabs(cy - y))*
          Image->Image[(cy*Image->Width + fx)*3 + c] +
   (1.0 - fabs(cx - x))*(1.0 - fabs(cy - y))*
          Image->Image[(cy*Image->Width + cx)*3 + c]
  ));
 }
 return Output;
}
//------------------------------------------------------------------------------

bool LoadTexture(const char* Filename){
 glEnable(GL_TEXTURE_2D);

 GLint MaxTextureSize;
 glGetIntegerv(GL_MAX_TEXTURE_SIZE, &MaxTextureSize);
 glPixelStorei(GL_PACK_ALIGNMENT  , 1);
 glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

 JPEG* Image = JPEG_Read(Filename);
 if(!Image){
  Message("Cannot read image");
  return false;
 }

 ImageWidth  = Image->Width;
 ImageHeight = Image->Height;

 // Textures must have a power-of-two size
 // Also limits the texture to the maximum texture size
 unsigned TextureWidth  = Image->Width;
 unsigned TextureHeight = Image->Height;

 int j = 1;
 while(j < (int)TextureWidth) j <<= 1;
 if(j <= (int)MaxTextureSize) TextureWidth = j;
 else                         TextureWidth = MaxTextureSize;

 j = 1;
 while(j < (int)TextureHeight) j <<= 1;
 if(j <= (int)MaxTextureSize) TextureHeight = j;
 else                         TextureHeight = MaxTextureSize;

 unsigned* Buffer = (unsigned*)malloc(TextureWidth*TextureHeight*sizeof(unsigned));
 int x, y;
 for(y = 0; y < TextureHeight; y++){
  for(x = 0; x < TextureWidth; x++){
   Buffer[y*TextureWidth + x] = Interpolate(
    Image,
    (double)x/(double)(TextureWidth -1),
    (double)y/(double)(TextureHeight-1)
   );
  }
 }
 JPEG_Delete(Image);

 // Bind the texture
 GLuint Index;
 glGenTextures(1, &Index);
 glBindTexture(GL_TEXTURE_2D, Index);

 // Load the texture
 glTexImage2D(
  GL_TEXTURE_2D,
  0,
  GL_RGBA,
  TextureWidth,
  TextureHeight,
  0,
  GL_RGBA,
  GL_UNSIGNED_BYTE,
  Buffer
 );
 free(Buffer);

 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

 glUniform1i(uWidth , ImageWidth);
 glUniform1i(uHeight, ImageHeight);
 glUniform1i(uImage , 0);
// glUniform1i(uToggle, Toggle);

 return true;
}
//------------------------------------------------------------------------------

unsigned ExtractVersion(const char* s){
 if(!s) return 0;

 unsigned major   = 0;
 unsigned minor   = 0;
 unsigned release = 0;
 unsigned j       = 0;

 while(s[j]){
  if(s[j] < '0' || s[j] > '9') break;
  major *= 10;
  major += s[j++] - '0';
 }

 if(s[j] == '.'){
  j++;
  while(s[j]){
   if(s[j] < '0' || s[j] > '9') break;
   minor *= 10;
   minor += s[j++] - '0';
  }

  if(s[j] == '.'){
   j++;
   while(s[j]){
    if(s[j] < '0' || s[j] > '9') break;
    release *= 10;
    release += s[j++] - '0';
   }
  }
 }

 while(minor   > 9) minor   /= 10;
 while(release > 9) release /= 10;

 return major*100 + minor*10 + release;
}
//------------------------------------------------------------------------------

bool InitGLEW(){
 char s[0x1000];

 GLenum Error = glewInit();
 if(Error != GLEW_OK){
  sprintf(s, "Error initialising glew: %s\n", glewGetErrorString(Error));
  Message(s);
  return false;
 }

 GL_Version = ExtractVersion((const char*)glGetString(GL_VERSION));

 if(GL_Version >= 210){
  GLSL_Version = ExtractVersion(
   (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION)
  );
 }else{
  GLSL_Version = 0;
 }

 sprintf(
  s,
  "Vendor: %s\n"
  "Renderer: %s\n"
  "OpenGL: %0.2f\n"
  "GLSL: %0.2f",
  glGetString(GL_VENDOR),
  glGetString(GL_RENDERER),
  GL_Version  /(float)100,
  GLSL_Version/(float)100
 );
 Message(s);

 if(GLSL_Version < 120){
  Message("Error: GLSL version < 3.3");
  return false;
 }

 return true;
}
//------------------------------------------------------------------------------
