#ifndef OpenCL_Wrapper_h
#define OpenCL_Wrapper_h
//------------------------------------------------------------------------------

#include "Global.h"
#include "JPEG_Wrapper.h"
//------------------------------------------------------------------------------

extern unsigned GL_Version;
extern unsigned GLSL_Version;

extern unsigned ImageWidth;
extern unsigned ImageHeight;

extern GLfloat Zoom; // The actual variable
extern GLint  uZoom; // The uniform handle

extern int Toggle; // The actual variable
extern GLint  uToggle; // The uniform handle

extern GLint  uTime; // The uniform handle

extern int    Variable; // The actual variable
extern GLint uVariable; // The uniform handle
//------------------------------------------------------------------------------

bool InitGLEW   ();
bool LoadShader (const char* VertexFile, const char* FragmentFile);
bool LoadTexture(const char* Filename);
//------------------------------------------------------------------------------

#endif
//------------------------------------------------------------------------------
