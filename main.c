#include "main.h"
//------------------------------------------------------------------------------

static bool Initialised;
//------------------------------------------------------------------------------

void Render(int Width, int Height){
 // Set the viewport (needs to be done every time for in case the window
 // was resized)
 glViewport(0, 0, Width, Height);
 glLoadIdentity();

 // Clear the colour buffer. You can also clear the depth, stencil and
 // other buffer here
 glClearColor(0, 0, 0, 0);
 glClear(GL_COLOR_BUFFER_BIT);

 // Calculate the coordinates (so that the image aspect ration is correct)
 GLfloat x, y;
 x = 1.0 * ((double)ImageWidth/ImageHeight) / ((double)Width/Height);
 y = 1.0;
 if(x > 1.0){
  x = 1.0;
  y = 1.0 / ((double)ImageWidth/ImageHeight) * ((double)Width/Height);
 }

 // Update the vertex buffer
 GLfloat Temp[16] = {
  -x, -y, 0.0, 0.0, // Vertex.x, Vertex.y, Texture.s, Texture.t
   x, -y, 1.0, 0.0,
   x,  y, 1.0, 1.0,
  -x,  y, 0.0, 1.0
 };

 glBufferData(
  GL_ARRAY_BUFFER,
  16*sizeof(GLfloat),
  Temp,
  GL_STATIC_DRAW
 );

 // Update the uniform variables
 GLfloat Time = GetClock() - StartTime; // Prevents overflow when rounding to
                                        // single precision
 glUniform1f(uZoom    , Zoom    );
 glUniform1i(uToggle    , Toggle);
 glUniform1f(uTime    , Time    );
 glUniform1i(uVariable, Variable);

 // Draw the image
 glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
//------------------------------------------------------------------------------

// Callback function to display rendering time (Time in seconds)
void RenderTime(double Time){
 static double interval = 0;

 if(GetClock()-interval > 1.0){
  interval = GetClock();
  char s[0x100];
  sprintf(
   s,
   "Zoom = %.3f; Variable = %d; Render time = %.3lf s; Frame-rate = %.3lf",
   Zoom, Variable, Time, 1.0/Time
  );
  Message(s);
 }
}
//------------------------------------------------------------------------------

void OnKeyDown(unsigned Key){
 switch(Key){
  case Key_E:
   Toggle = (Toggle + 1) % 2;
   printf("Toggle is %d\n", Toggle);
   break;
  
  case Key_I:
   if(Zoom < 10.0) Zoom *= 1.2;
   break;

  case Key_O:
   if(Zoom > 0.1) Zoom /= 1.2;
   break;

  case Key_J:
   if(Variable > 2) Variable -= 2;
   break;

  case Key_K:
   if(Variable < 99) Variable += 2;
   break;

  default:
   break;
 }
}
//------------------------------------------------------------------------------

#ifdef __WIN32__
LRESULT CALLBACK WndProc(
 HWND   hWnd,
 UINT   message,
 WPARAM wParam,
 LPARAM lParam
){
 RECT   rect;

 switch(message){
  case WM_CLOSE:
   PostQuitMessage(0);
   return 0;

  case WM_SIZE:
  case WM_MOVE:
  case WM_PAINT:
  case WM_MOVING:
  case WM_SIZING:
   if(Initialised){
    GetClientRect(hwnd, &rect);
    Render(rect.right, rect.bottom);
    SwapBuffers(hDC);
   }
   break;

  case WM_KEYDOWN:
   if(wParam == VK_ESCAPE) PostQuitMessage(0);
   else                    OnKeyDown(wParam);
   return 0;

  default:
   break;
 }
 return DefWindowProc(hWnd, message, wParam, lParam);
}
//------------------------------------------------------------------------------

int WINAPI WinMain(
 HINSTANCE hInstance,
 HINSTANCE hPrevInstance,
 LPSTR     lpCmdLine,
 int       nCmdShow
){
 Initialised = false;
 StartTime   = GetClock();

 double time = StartTime;

 WNDCLASSEX wcex;
 HGLRC      hRC;
 RECT       rect;
 MSG        msg;

 // register window class
 wcex.cbSize        = sizeof(WNDCLASSEX);
 wcex.style         = CS_OWNDC;
 wcex.lpfnWndProc   = WndProc;
 wcex.cbClsExtra    = 0;
 wcex.cbWndExtra    = 0;
 wcex.hInstance     = hInstance;
 wcex.hIcon         = LoadIcon(0, IDI_APPLICATION);
 wcex.hCursor       = LoadCursor(0, IDC_ARROW);
 wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
 wcex.lpszMenuName  = 0;
 wcex.lpszClassName = "GLSample";
 wcex.hIconSm       = LoadIcon(0, IDI_APPLICATION);;

 if(!RegisterClassEx(&wcex)) return 0;

 // create main window
 hwnd = CreateWindowEx(
  0,
  "GLSample",
  "OpenGL Sample",
  WS_OVERLAPPEDWINDOW,
  CW_USEDEFAULT, CW_USEDEFAULT,
  640, 480,
  0,
  0,
  hInstance,
  0
 );

 ShowWindow(hwnd, nCmdShow);

 // enable OpenGL for the window
 PIXELFORMATDESCRIPTOR pfd;

 int iFormat;

 // get the device context (DC)
 hDC = GetDC(hwnd);

 // set the pixel format for the DC
 ZeroMemory(&pfd, sizeof(pfd));

 pfd.nSize      = sizeof(pfd);
 pfd.nVersion   = 1;
 pfd.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
 pfd.iPixelType = PFD_TYPE_RGBA;
 pfd.cColorBits = 24;
 pfd.cDepthBits = 0;
 pfd.iLayerType = PFD_MAIN_PLANE;

 iFormat = ChoosePixelFormat(hDC, &pfd);

 SetPixelFormat(hDC, iFormat, &pfd);

 // create and enable the render context (RC)
 hRC = wglCreateContext(hDC);

 wglMakeCurrent(hDC, hRC);

 if(!InitGLEW()){
  wglMakeCurrent(0, 0);
  wglDeleteContext(hRC);
  ReleaseDC(hwnd, hDC);
  DestroyWindow(hwnd);
  return 1;
 }

 if(!LoadShader ("..\\OpenGL\\Texture.vp", "..\\OpenGL\\Texture.fp")) return 1;
 if(!LoadTexture("..\\Pic\\god_of_war_3.jpg")) return 1;
 Initialised = true;

 // program main loop
 while(true){
  while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE)){
   if(msg.message == WM_QUIT) break;
   TranslateMessage(&msg);
   DispatchMessage (&msg);
  }
  if(msg.message == WM_QUIT) break;

  GetClientRect(hwnd, &rect);
  Render(rect.right, rect.bottom);
  SwapBuffers(hDC);
  RenderTime(GetClock()-time);
  time = GetClock();
  Sleep(1);
 }
 wglMakeCurrent(0, 0);
 wglDeleteContext(hRC);
 ReleaseDC(hwnd, hDC);
 DestroyWindow(hwnd);

 return msg.wParam;
}
#endif // __WIN32__
//------------------------------------------------------------------------------

#ifdef __unix__
// From <https://www.opengl.org/wiki/Programming_OpenGL_in_Linux:_GLX_and_Xlib>
int main(int argc, char** argv){
 Initialised = false;
 StartTime   = GetClock();

 double time;

 Display*             dpy;
 Window               root;
 XVisualInfo*         vi;
 Colormap             cmap;
 XSetWindowAttributes swa;
 Window               win;
 GLXContext           glc;
 XWindowAttributes    gwa;
 XEvent               xev;

 GLint att[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};

 dpy = XOpenDisplay(0);

 if(dpy == 0){
  printf("\n\tcannot connect to X server\n\n");
  return 1;
 }

 root = DefaultRootWindow(dpy);

 vi = glXChooseVisual(dpy, 0, att);

 if(vi == 0){
  printf("\n\tno appropriate visual found\n\n");
  return 1;
 }else{
  // %p creates hexadecimal output like in glxinfo
  printf("\n\tvisual %p selected\n", (void*)vi->visualid);
 }

 cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);

 swa.colormap = cmap;
 swa.event_mask = ExposureMask | KeyPressMask;

 win = XCreateWindow(
  dpy,
  root,
  0, 0, 640, 480, 0,
  vi->depth,
  InputOutput,
  vi->visual,
  CWColormap | CWEventMask,
  &swa
 );

 XMapWindow(dpy, win);
 XStoreName(dpy, win, "OpenGL Sample");

 glc = glXCreateContext(dpy, vi, 0, GL_TRUE);
 glXMakeCurrent(dpy, win, glc);

 if(!InitGLEW()){
  glXMakeCurrent   (dpy, None, 0);
  glXDestroyContext(dpy, glc);
  XDestroyWindow   (dpy, win);
  XCloseDisplay    (dpy);
  return 1;
 }
 if(!LoadShader ("OpenGL/Texture.vp", "OpenGL/Texture.fp")) return 1;
 if(!LoadTexture("Pic/god_of_war_3.jpg")) return 1;
 Initialised = true;

 bool running = true;
 while(running){
  while(XCheckWindowEvent(dpy, win, 0xFFFFFFFF, &xev)){
   switch(xev.type){
    case Expose:
     XGetWindowAttributes(dpy, win, &gwa);
     Render(gwa.width, gwa.height);
     glXSwapBuffers(dpy, win);
     break;

    case KeyPress:
      printf("KeyPress: keycode %u state %u\n", xev.xkey.keycode, xev.xkey.state);
     if(xev.xkey.keycode == Key_Escape) running = false;
     else OnKeyDown(xev.xkey.keycode);
     break;
   }
  }
  XGetWindowAttributes(dpy, win, &gwa);
  Render(gwa.width, gwa.height);
  glXSwapBuffers(dpy, win);
  RenderTime(GetClock()-time);
  time = GetClock();
  usleep(1000);
 }
 glXMakeCurrent   (dpy, None, 0);
 glXDestroyContext(dpy, glc);
 XDestroyWindow   (dpy, win);
 XCloseDisplay    (dpy);

 return 0;
}
#endif // __unix__
//------------------------------------------------------------------------------
