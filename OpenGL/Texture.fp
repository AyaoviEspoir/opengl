varying vec2 TextureCoord;
//------------------------------------------------------------------------------

uniform float     Time;
float     		  zoom_factor;
uniform int       Width;
uniform int       Height;
uniform int       Variable;
uniform int       Toggle;
uniform sampler2D Image;
//------------------------------------------------------------------------------

void main(void) {
	float LensR = float(Variable) / 200.0; // Radius
	vec2  LensP = vec2(sin(Time), sin(0.271*Time)) / 2.0 + 0.5; // center of the lens
	zoom_factor = 2.;
	vec2 R = TextureCoord - LensP;
	R.x *= float(Width)/float(Height);
	
	//printf("Toggle is %d", Toggle);
	if (Toggle == 0) {
		if(length(R) < LensR) {
			vec2 pixel = (TextureCoord / zoom_factor) + (LensP - (LensP / zoom_factor));
			gl_FragColor = texture2D(Image, pixel);
		}

		// Remember to scale R back to texture space so that the 
		// aspect ratio of the lens image is correct
		else { gl_FragColor = texture2D(Image, TextureCoord); }
	}
	
	else if (Toggle == 1) {
		zoom_factor = 2.5;
		if(length(R) < LensR) {
			// Example square-window blur function
			vec2 pixelCoord = TextureCoord*vec2(Width-1, Height-1);
			vec4 pixel = vec4(0, 0, 0, 0);
			int x, y;
			
			for(y = -1; y < 2; y++) {
				for(x = -1; x < 2; x++) {
				if (x == 0 && y == 0)
					pixel += 8. * texture2D(Image, pixelCoord / vec2(Width-1, Height-1));
					//pixel += 8. * texture2D(Image, ((pixelCoord / zoom_factor) + (LensP - (LensP / zoom_factor))) / vec2(Width-1, Height-1));
				else
					pixel -= texture2D(Image, (pixelCoord + vec2(x, y)) / vec2(Width-1, Height-1));
					//pixel -= texture2D(Image, (((pixelCoord + vec2(x, y)) / zoom_factor) + (LensP - (LensP / zoom_factor))) / vec2(Width-1, Height-1));
				}
			}
			gl_FragColor = vec4(pixel.rgb, 1);
		}
		else { gl_FragColor = texture2D(Image, TextureCoord); }
	}
}

//------------------------------------------------------------------------------
